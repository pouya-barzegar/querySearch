module Trie (
      contains
    , empty
    , insert
    , lookup
    , null
    , Trie ) where

-- defining a data structure to use
-- main phase

import qualified Data.Map as M
import Data.Map ((!), fromList)
import Prelude hiding (null, lookup)
import Data.Maybe (fromMaybe)

-- TODO defining insertion, search : Done

newtype Trie a = Trie (M.Map a (Trie a)) deriving Show

-- data Trie a = Trie { value :: Maybe a,
--                      children :: M.Map a (Trie a) }

empty :: Ord a => Trie a
empty = Trie M.empty

null :: Ord a => Trie a -> Bool
null (Trie m) = M.null m

insert :: Ord a => [a] -> Trie a -> Trie a
insert [] t = t
insert (x:xs) (Trie m) = Trie m'
    where m' = M.alter t' x m
          t' = Just . insert xs . fromMaybe empty

-- contains certain prefix? For bonus , if any!
contains :: Ord a => [a] -> Trie a -> Bool
contains [] _ = True
contains (x:xs) (Trie tr) = case M.lookup x tr of
    Just t -> contains xs t
    Nothing -> False

-- return subtrie, raise error if not exists that prefix
lookup :: Ord a => [a] -> Trie a -> Trie a
lookup [] t = t
lookup (x:xs) t@(Trie m) = lookup xs $ M.findWithDefault t x m
