module Lib
    ( upIt
    , shortLn
    , wordList
    , openAddr
    , getHandles
    , unique
    , counter
    ) where

import           Data.Char
import           Data.List
import           System.Directory
import           System.IO
import           Trie

upIt :: String -> IO ()
upIt = putStrLn . map toUpper


shortLn :: String -> String
shortLn inp =
        let
            allines = lines inp
            shortlines = filter (\ln -> length ln < 20) allines
        in unlines shortlines

wordList :: String -> [String]
wordList content =
        let
            docword = words content
        in docword

openAddr :: FilePath -> IO Handle
openAddr address =
        let
            file = address
        in openFile file ReadMode

getHandles :: [FilePath] -> [IO Handle]
getHandles = map openAddr

rightContent cnt = init $ tail cnt

-- middle Step
-- processing all input
joinAllList :: [[String]] -> [String]
joinAllList = undefined

convertZip :: [String] -> [(Int, String)]
convertZip = undefined

rmDups :: Ord a => [a] -> [a]
rmDups  = map head . group . sort

-- arrRep :: [String] -> [Int]
arrRep [] = []
arrRep [x] = [1]
arrRep al@(x:y:xys)
    | x == y =  count al : arrRep ( y:xys )
    | otherwise = 1 : arrRep ( y:xys )

count [] = 0
count [x] = 1
count (x:y:xs) = 1 + count (y:xs)

unique :: Eq a => [a] -> [a]
unique x = map head (group x)

counter :: Eq a => [a] -> [a] -> Int
counter _ [] = 0
counter [] _ = 0
counter query@(x:xs) lst
    | x `elem` lst = 1 + counter query (delete x lst)
    | otherwise = counter xs lst
