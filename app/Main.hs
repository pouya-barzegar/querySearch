{-# LANGUAGE ScopedTypeVariables #-}
module Main where
import           Trie
import           Control.Monad
import           Data.Char
import           Data.List
import           Lib
import           System.Directory
import           System.Environment
import           System.IO
import qualified Data.Map as M
import           Data.Map ((!), fromList)
import           Prelude hiding (null, lookup)
import           Data.Maybe (fromMaybe)

main :: IO ()

main = do
    docAddr <- getArgs

    -- a simple pwd
    print "we are at:"
    dir <- getCurrentDirectory
    putStrLn dir
    -- getting doc addresses as args
    -- given a query;
    putStr "Enter Query:\n"
    query <- getLine
    let qwords = words query
    print qwords

    -- reading the files given:
    let handleList = getHandles docAddr
    docList <- mapM readFile docAddr
    -- print docList
    let allWords = map words docList
    -- print allWords

    let wordsList = sort $ concat allWords
    -- print wordsList
    -- next, doing something with all of this! :)

    -- Remove all repeats
    -- Once for the concated, once for each
    let allUniqWords = unique wordsList
        eachUniqWords = map unique allWords
    -- print allUniqWords
    -- print eachUniqWords
    let answer = map (counter qwords) eachUniqWords

    print answer
    -- Works! XD
    -- TODO searching the files
    -- let tri = empty

    -- if it was found , print the file name.
    let val = elemIndex (maximum answer) answer

    print (show (answer !! 0) ++ ":::" ++ docAddr !! 0)
    print (show (answer !! 1) ++ ":::" ++ docAddr !! 1)
    print (show (answer !! 2) ++ ":::" ++ docAddr !! 2)
    putStrLn "finished"
